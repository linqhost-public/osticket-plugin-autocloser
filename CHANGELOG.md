# Release notes
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

#### 2.1.0
- Bug: Ticket reply was added to thread but not mail send
- Improvement: When a canned response is send and HTML is not enabled in Osticket. The HTML will be converted to text (hence the bundled soundasleep/html2text library)
- Improvement: Enabling debugging can be set in the plugin settings instead in the plugin source code
- Improvement: Removed the dos newlines :-)

#### 2.0.0
- Original version from https://github.com/clonemeagain/plugin-autocloser/