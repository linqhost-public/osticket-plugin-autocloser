<?php return array(
 'id' => 'linqhost:autocloser', # notrans
 'version' => '2.1.0',
 'name' => 'Ticket Closer',
 'author' => 'no-reply@linqhost.nl',
 'description' => 'Changes ticket statuses based on age.',
 'url' => 'https://gitlab.com/linqhost-public/osticket-plugin-autocloser',
 'plugin' => 'class.CloserPlugin.php:CloserPlugin'
);
